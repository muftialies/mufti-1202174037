<?php 
	$nama = $_GET['nama_driver'];
	$notelp = $_GET['notelp_driver'];
	$tanggal = $_GET['tanggal_driver'];
	$jenis = $_GET['jenis_driver'];
	
	if (empty($_GET['tambahan_driver'])) {
		$tambahan = "Tidak";
	} else{
		$tambahan = "Ya";
	}
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Menu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<style type="text/css">
		
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 text-center">
				<h1 style="margin: 30px 0px 0px 0px;">~Data Driver Ojol~</h1><br>
				<h6>Nama</h6>
				<label><?php echo $nama; ?></label>
				<br><br>

				<h6>Nomor Telepon</h6>
				<label><?php echo $notelp; ?></label>
				<br><br>

				<h6>Tanggal</h6>
				<label><?php echo $tanggal; ?></label>
				<br><br>

				<h6>Asal Driver</h6>
				<label><?php echo $jenis; ?></label>
				<br><br>

				<h6>Bawa Kantong</h6>
				<label><?php echo $tambahan; ?></label><br>
				<br>
				<a href="form.html"><button class="btn btn-primary">Kembali</button></a>
			</div>
			<div class="col-sm-5 text-center">
				<h1 style="margin: 30px 0px 0px 0px;">~Menu~</h1><br>
				<label>Pilih Menu</label><br><br>
				<form onsubmit="yakin()" action="nota.php" method="post">
				<div class="row">
					<div class="col-sm-6 text-left">
						<h6><input type="checkbox" name="esCoklatSusu" value="menu1"> Es Coklat Susu</h6>		
					</div>
					<div class="col-sm-6">
						<span>Rp. 28.000,-</span>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6 text-left">
						<h6><input type="checkbox" name="esSusuMatcha" value="menu2"> Es Susu Matcha</h6>		
					</div>
					<div class="col-sm-6">
						<span>Rp. 18.000,-</span>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6 text-left">
						<h6><input type="checkbox" name="esSusuMojicha" value="menu3"> Es Susu Mojicha</h6>		
					</div>
					<div class="col-sm-6">
						<span>Rp. 15.000,-</span>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6 text-left">
						<h6><input type="checkbox" name="esMatchaLatte" value="menu4"> Es Matcha Latte</h6>
					</div>
					<div class="col-sm-6">
						<span>Rp. 30.000,-</span>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6 text-left">
						<h6><input type="checkbox" name="esTaroSusu" value="menu5"> Es Taro Susu</h6>
					</div>
					<div class="col-sm-6">
						<span>Rp. 21.000,-</span>
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Nomor Order</h6>	
					</div>
					<div class="col-sm-8">
						<input type="number" name="noOrderMenu" required style="width: 250px;">
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Nama Pemesan</h6>
					</div>
					<div class="col-sm-8">
						<input type="text" name="namaPemesan" required style="width: 250px;">
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Email</h6>
					</div>
					<div class="col-sm-8">
						<input type="email" name="email" required style="width: 250px;">
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Alamat Order</h6>	
					</div>
					<div class="col-sm-8">
						<input type="text" name="alamatOrder" required style="width: 250px;">
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Member</h6>
					</div>
					<div class="col-sm-8">
						<input type="radio" name="member" value="Ya" required> Ya
						<input type="radio" name="member" value="Tidak" required> Tidak
					</div>
				</div>
				<hr>
				<div class="row text-left">
					<div class="col-sm-4 label">
						<h6>Metode Pembayaran</h6>
					</div>
					<div class="col-sm-8">
						<select name="metodePembayaran" required >
							<option selected="true" disabled="true" value="" >Pilih Metode Pembayaran</option>
							<option value="cash">Cash</option>
							<option value="eMoney">E-Money (OVO/Gopay)</option>
							<option value="credit">Credit Card</option>
							<option value="other">Lainnya</option>
						</select>
					</div>
				</div>
				<hr>
				<div class="row text-center">
					<div class="col-sm-12">
						<input type="submit" name="submitMenu" value="CETAK STRUK" id="submitMenu" class="btn btn-success" style="width: 100%;">
						<input type="text" name="totalHarga" id="hasil" hidden>
					</div>
				</div>
				<hr>
				</form>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		$("#submitMenu").click(function(){
			var total = 0;

			if ($("input[name='esCoklatSusu']:checked").length > 0) {
				total += 28000;
			}

			if ($("input[name='esSusuMatcha']:checked").length > 0) {
				total += 18000;
			}

			if ($("input[name='esSusuMojicha']:checked").length > 0) {
				total += 15000;
			}

			if ($("input[name='esMatchaLatte']:checked").length > 0) {
				total += 30000;
			}

			if ($("input[name='esTaroSusu']:checked").length > 0) {
				total += 21000;
			}

			if ($("input[name='member']:checked").val() == "Ya") {
				total *= 0.9;
			}			

			$("#hasil").val(total);
		});
	});

	function yakin() {
		if(!confirm('Apakah Anda Yakin?')){
			return false;
		}
	}
</script>
</html>