
<?php 
	$harga = $_POST['totalHarga'];
	$judul = array("ID","Nama","Email","Alamat","Member","Pembayaran");
	$isi = array($_POST['noOrderMenu'],$_POST['namaPemesan'],$_POST['email'],$_POST['alamatOrder'],$_POST['member'],$_POST['metodePembayaran']);
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Struk Belanja</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-6 text-center">
				<h1 style="margin: 20px 0px 10px 0px;">Transaksi Pemesanan</h1>
				<label >Terima kasih telah berbelanja dengan Kopi Susu Duarrr!</label>
				<h1 style="margin: 10px 0px 20px 0px;">Rp. <?php echo $harga; ?>.00,-</h1>
				<hr>
				<?php 
					for ($i=0; $i < 6; $i++) { ?>
						<div class="row text-left">
							<div class="col-sm-2"></div>
							<div class="col-sm-4">
								<h6><?php echo $judul[$i]; ?></h6>
							</div>
							<div class="col-sm-4">
								<label><?php echo $isi[$i];?></label>
							</div>
							<div class="col-sm-2"></div>

						</div>
					<hr>
				<?php }?>
			</div>
			<div class="col-sm-3"></div>
		</div>
	</div>
</body>
</html>