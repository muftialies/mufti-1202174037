@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($post as $isi)
            @if ($isi->id == 6 && Auth::user()->id != 1)
                @continue
            @endif
        <div class="col-md-4">
            <div class="card mx-auto my-3" style="width: 340px;">
                <div class="card-header">
                    <img src="{{$isi->avatar}}" width="50" style="border-radius: 50%; border: solid 1px;">
                    {{$isi->name}}</div>

                <div class="card-body">
                    <img src="{{$isi->image}}" width="300">
                </div>
                <div class="card-footer">
                    <b>{{$isi->email}}</b><br>
                    {{$isi->caption}}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
