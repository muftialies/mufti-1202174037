<?php

use Illuminate\Database\Seeder; 
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Str;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('post')->insert([ 
            'user_id' => 2,
            'caption' => 'NGAJI KITAB KUY', 
            'image' => 'KMNUNgajiKitab.jpeg', 
        ]); 

        DB::table('post')->insert([ 
            'user_id' => 2,
            'caption' => 'J A K E T D A Y', 
            'image' => 'KMNUJaketDay.jpeg', 
        ]); 

        DB::table('post')->insert([ 
            'user_id' => 2,
            'caption' => 'SHOLAWATAN KITAB AL BARZANJI', 
            'image' => 'KMNUSholawatan.jpeg', 
        ]); 

        DB::table('post')->insert([ 
            'user_id' => 2,
            'caption' => 'DZIKIR dan DOA BERSAMA// ISTIQOMAH', 
            'image' => 'KMNUIstigosah.jpeg', 
        ]);

        DB::table('post')->insert([ 
            'user_id' => 1,
            'caption' => 'FOTO PROFILE MUF', 
            'image' => 'profile.jpg', 
        ]); 

        DB::table('post')->insert([ 
            'user_id' => 1,
            'caption' => 'NONTON KONSER CUYY', 
            'image' => 'konser.jpg', 
        ]); 
    }
}
