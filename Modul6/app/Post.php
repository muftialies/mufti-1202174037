<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'caption', 'image', 'likes',
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }

    public function kometarPosts()
    {
        return $this->hasMany(\App\KomentarPost::class,'post_id');   
    }
}
