<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; 
use App\Post; 
use App\KomentarPost; 
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     /*   $hasil = User::with('posts')->where('id', Auth::User()->id)->get();
        // $hasil = Post::where('user_id', Auth::User()->id)->get();
        // dd($hasil);
        return view('profile',compact('hasil'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() //add post
    {
        return view('addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) //simpan
    {
        // return $request;
        if ($request->create_komentar == "kirim") {

            $komentars = new KomentarPost();
            $komentars->user_id = Auth::User()->id;
            $komentars->post_id = $request->post_id;
            $komentars->comment = $request->comment;

            $komentars->save();
            return redirect()->route('post.show',$request->post_id);
        } else {
            $file = $request->file('post_image');

            if (!$file) {
                return redirect()->route('user.index');
            }

            $file_name = $file->getClientOriginalName();
            $path = public_path("/posts/".Auth::User()->id);
            $file->move($path, $file_name);

            $posts = new Post();
            $posts->user_id = Auth::User()->id;
            $posts->caption = $request->post_caption;
            $posts->image = "/posts/".Auth::User()->id."/".$file_name;
            $posts->likes = 0;

            $posts->save();

            return redirect()->route('user.show',Auth::User()->id);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) //detail
    {
        $hasil = User::with('posts')->where('id', $id)->get();
        // $hasil = Post::where('user_id', Auth::User()->id)->get();
        // dd($hasil);
        return view('profile',compact('hasil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) // editprofile
    {
        $edit = User::where('id', Auth::User()->id)->get();
        // dd($edit);
        return view('editprofile',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) //updatenya
    {
        $file = $request->file('profile_image');

        $users = User::findOrFail($id);
        $users->title = $request->profile_title;
        $users->description = $request->profile_desc;
        $users->url = $request->profile_url;

        if ($file) {
            $file_name = $file->getClientOriginalName();
            $path = public_path("/avatars/".Auth::User()->id);
            $file->move($path, $file_name);

            $users->avatar = "/avatars/".Auth::User()->id."/".$file_name;
        }

        $users->save();
        return redirect()->route('user.show',Auth::User()->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
