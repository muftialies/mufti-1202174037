<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class LikeController extends Controller
{
    public function like($id, $like)
    {
    	// return $like;
        $posts = Post::findOrFail($id);

        $posts->likes = $like + 1;

        $posts->save();
        return redirect()->route('post.show',$id);
    }
}
