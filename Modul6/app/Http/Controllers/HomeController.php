<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post; 
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $hasil = Post::where('user_id', Auth::User()->id)->get();
        $hasil = Post::with('kometarPosts')->with('user')->get();
        $komentars = DB::table('komentar_posts')
            ->join('users', 'users.id', '=', 'komentar_posts.user_id')
            ->get();
        // dd($komentars);
        return view('home',compact('hasil','komentars'));
    }
}
