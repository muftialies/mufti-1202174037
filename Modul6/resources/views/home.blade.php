@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($hasil as $isi)
        <div class="col-md-7">
            <div class="card mx-auto my-3" style="width: 100%;">
                <div class="card-header">
                    <a href="{{ route('user.show',$isi->user_id) }}">
                        <img src="{{$isi->user->avatar}}" width="50" style="border-radius: 50%; border: solid 1px;">
                    </a>
                    {{$isi->user->name}}
                </div>

                <div class="card-body">
                    <a href="{{ route('post.show', $isi->id) }}">
                        <img src="{{$isi->image}}" width="100%">
                    </a>
                </div>
                <div class="card-footer">
                    <a href="{{ url('like/'.$isi->id.'/'.$isi->likes) }}"><i class="fa fa-heart-o" style="font-size:24px; color: black;"></i></a> 
                    <a href="#home_komentar{{$isi->id}}"><i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i></a><br>
                    <p><b>{{$isi->likes}} Like</b><br>
                    <b>{{$isi->user->email}}</b> {{$isi->caption}}</p>
                    @foreach($komentars as $komen)
                        @if($komen->post_id == $isi->id)
                            <b>{{$komen->email}}</b> {{$komen->comment}}<br>
                        @endif
                    @endforeach
                    <form action="{{route('user.store')}}" method="POST"> 
                        @csrf 
                        <div class="input-group">
                            <input type="hidden" name="post_id" value="{{$isi->id}}">
                            <input type="text" class="form-control" name="comment" id="home_komentar{{$isi->id}}" required placeholder="Add a comment..">
                            <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Post</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
