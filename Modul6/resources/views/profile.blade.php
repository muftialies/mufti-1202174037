@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-5">
        <div class="col-md-4 text-center">
            <img src="{{asset($hasil[0]->avatar)}}" width="60%">
        </div>
        <div class="col-md-6">
            <h4>{{$hasil[0]->name}}</h4><br>
            <p>
                @if($hasil[0]->id == Auth::User()->id)
                    <a href="{{ route('user.edit', [Auth::user()->id]) }}" >Edit Profile</a><br>
                @endif
                <b>{{count($hasil[0]->posts)}}</b> Post</p>
            <p><b>{{$hasil[0]->title}}</b><br>{{$hasil[0]->description}} <br><a href="http://{{$hasil[0]->url}}" target="_blank">{{$hasil[0]->url}}</a> 
            </p>
        </div>
        <div class="col-md-2 text-right">
            @if($hasil[0]->id == Auth::User()->id)
                <a href="{{ route('user.create') }}" >Add New Post</a>
            @endif
        </div>
    </div>
    <div class="row justify-content-left my-1">
        @foreach($hasil[0]->posts as $post)
        <div class="col-md-4 my-3">
            <a href="{{ route('post.show', $post->id) }}"><img src="{{asset($post->image)}}" width="100%"></a>
        </div>
        @endforeach
    </div>
</div>
@endsection
