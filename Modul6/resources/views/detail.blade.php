@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <img src="{{asset($detail[0]->image)}}" width="100%">
        </div>
        <div class="col-md-5">
            <a href="{{ route('user.show',$detail[0]->user_id) }}">
            <img src="{{asset($detail[0]->user->avatar)}}" width="50" style="border-radius: 50%; border: solid 1px;">
            </a> <b>{{$detail[0]->user->name}}</b> <br>
            <hr>
            <p><b>{{$detail[0]->user->email}}</b> {{$detail[0]->caption}}</p>
            <p>
                @foreach($comments as $komentar)
                <b>{{$komentar->user->email}}</b> {{$komentar->comment}} <br>
                @endforeach
            </p>
            <hr>
            <!-- <button class="like">Like</button> -->
            <a href="{{ url('like/'.$detail[0]->id.'/'.$detail[0]->likes) }}"><i class="fa fa-heart-o" style="font-size:24px; color: black;"></i></a> 
            <a href="#detail_komentar"><i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i></a><br>
            <p><b>{{$detail[0]->likes}} Like</b></p>
            <form action="{{route('user.store')}}" method="POST">  
                @csrf
                <input type="hidden" name="post_id" value="{{$detail[0]->id}}">
                <div class="input-group">
                    <input type="text" class="form-control" name="comment" id="detail_komentar" required placeholder="Add a comment..">
                    <button type="submit" class="btn btn-primary" name="create_komentar" value="kirim">Send</button>
                </div>  
            </form>    
        </div>
    </div>
</div>
@endsection
