@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center my-1">
        <div class="col-md-9">
            <form method="POST" action="{{route('user.store')}}"  enctype="multipart/form-data">
                @csrf
                <h2>Add New Post</h2>
                <p>Post Caption</p>
                <input type="text" class="form-control" name="post_caption" required><br>
                <p>Post Image</p>
                <input type="file" class="form-control-file" name="post_image" required placeholder="Foto">
                <br>
                <button type="submit" class="btn btn-primary">Add New Post</button>
            </form>
        </div>
    </div>  
</div>
@endsection
