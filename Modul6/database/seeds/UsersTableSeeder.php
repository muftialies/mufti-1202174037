<?php 
use Illuminate\Database\Seeder; 
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Str; 
class UserTableSeeder extends Seeder 
{ 
    public function run() 
    { 
        DB::table('users')->insert([ 
            'name' => 'Mufti Alie Satriawan', 
            'email' => 'muftialies@student.telkomuniversity.ac.id', 
            'password' => bcrypt('qwert12345'), 
            'avatar' => 'profile.jpg', 
        ]); 

        DB::table('users')->insert([ 
            'name' => 'KMNU Tel-U', 
            'email' => 'kmnutelu@gmail.com', 
            'password' => bcrypt('qwertyuiop'), 
            'avatar' => 'KMNULogo.jpg', 
        ]); 
    } 
} 