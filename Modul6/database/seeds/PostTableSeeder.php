<?php

use Illuminate\Database\Seeder; 
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Str;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('posts')->insert([ 
            'user_id' => 2,
            'caption' => 'NGAJI KITAB KUY', 
            'image' => 'KMNUNgajiKitab.jpeg', 
            'likes' => 0, 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 2,
            'caption' => 'J A K E T D A Y', 
            'image' => 'KMNUJaketDay.jpeg', 
            'likes' => 0, 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 2,
            'caption' => 'SHOLAWATAN KITAB AL BARZANJI', 
            'image' => 'KMNUSholawatan.jpeg', 
            'likes' => 0, 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 2,
            'caption' => 'DZIKIR dan DOA BERSAMA// ISTIQOMAH', 
            'image' => 'KMNUIstigosah.jpeg', 
            'likes' => 0, 
        ]);

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'FOTO PROFILE MUF', 
            'image' => 'profile.jpg', 
            'likes' => 0, 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'NONTON KONSER CUYY', 
            'image' => 'konser.jpg', 
            'likes' => 0, 
        ]); 
    }
}
