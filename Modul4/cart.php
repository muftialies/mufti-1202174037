<?php 
	session_start();
	if (!(isset($_SESSION["status"]) && $_SESSION["status"]=="login")) {
		header("Location: home.php");
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="dicoding.png">
	<title>Cart</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #2196F3;">
	  <a class="navbar-brand" href="home.php"><img src="EAD.png" width="100"></a>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 63rem;">
	    <ul class="navbar-nav mr-auto float-right">
	      <li class="nav-item active">
	        <a class="nav-link text-white" href="cart.php"><img src="shopping-cart.png" width="20"></a>
	      </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="profile.php">Profile</a>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="logout.php">Log Out</a>
	        </div>
	      </li>
	    </ul>
	  </div>
	</nav>
	<h3 class="text-center my-3">Cart</h3>
	<div class="table-responsive">
	<table class="table mx-auto mt-5" style="width: 58rem;">
	  <thead>
	    <tr>
	      <th scope="col">No</th>
	      <th scope="col">Product</th>
	      <th scope="col">Price</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php 
	  		$koneksi = mysqli_connect('localhost','root','','db_wad_04');
	  		$idUser = $_SESSION['id_user'];

			$login = "SELECT * FROM `cart` WHERE `user_id`='".$idUser."'";

			$query = mysqli_query($koneksi, $login);	

			$no = 0;
			$i = 1;

			if (mysqli_num_rows($query) > 0) {
				while ($hasil = mysqli_fetch_assoc($query)) {
				?>
				<tr>
			      <th scope="row"><?php echo $i; $i++;?></th>
			      <td><?php echo $hasil["product"]; ?></td>
			      <td>Rp <?php echo number_format($hasil["price"],0,",","."); $no += $hasil["price"];?>,-</td>
			      <td><a href="crudCart.php?delete=delete&idCart=<?php echo $hasil["id"]; ?>" class="btn btn-danger">Delete</a></td>
			    </tr>
			<?php	}
			} else {?>
				<tr>
			      <th scope="row" colspan="4" class="text-center">Cart Tidak ada</th>
			    </tr>
			<?php
			}
	  	 ?>
	    <tr>
	      <th scope="row" colspan="2" class="text-center">Total</th>
	      <th>Rp <?php echo number_format($no,0,",","."); ?>,-</th>
	      <td></td>
	    </tr>
	  </tbody>
	</table>
	</div>
	<footer class="page-footer font-small dark pt-4">
		<div class="footer text-center py-3">
			© EAD STORE<a href="#"> MUF</a>
		</div>
	</footer>
</body>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>