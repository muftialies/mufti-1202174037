<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<link rel="icon" href="dicoding.png">
	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #2196F3;">
	  <a class="navbar-brand" href="home.php"><img src="EAD.png" width="100"></a>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 63rem;">
	    <ul class="navbar-nav mr-auto float-right">
	    	<?php if (isset($_SESSION["status"]) && $_SESSION["status"]=="login") {?>
	    		<li class="nav-item active">
			    	<a class="nav-link text-white" href="cart.php"><img src="shopping-cart.png" width="20"></a>
			    </li>
			    <li class="nav-item dropdown active">
			    	<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
			        	<a class="dropdown-item" href="profile.php">Profile</a>
			          	<div class="dropdown-divider"></div>
			          	<a class="dropdown-item" href="logout.php">Log Out</a>
			        </div>
		     	</li>
	    	<?php } else {?>
	    		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#loginModal" data-whatever="@login">Login</a>
	      		</li>
	      		<li class="nav-item active">
	        		<a class="nav-link text-white" href="#" data-toggle="modal" data-target="#registerModal" data-whatever="@register">Register</a>
	      		</li>
	    	<?php } ?>
	    </ul>
	  </div>
	</nav>
	<div class="card my-3 mx-auto text-white" style="width: 58rem; background-color: #2196F3;">
	  <div class="card-body">
	    <span class="align-middle">
	    	<h1>Hello Coders</h1>
	    	<p>Welcome to our store,  please a look for the product you might buy</p>
	    </span>
	  </div>
	</div>
	<div class="row mx-auto" style="width: 60rem;">
  		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 18rem;">
			  <img src="https://www.dicoding.com/images/original/academy/web_fundamental_logo_030519090933.jpg" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Belajar Dasar Pemrograman Web</h5>
			    <p class="card-text"><b>Rp.100.000,-</b></p>
			    <p class="card-text" style="height: 5rem;">Website adalah halaman informasi yang bisa diakses oleh siapapun dari seluruh penjuru dunia dengan menggunakan koneksi internet.</p>
			  </div>
			  <div class="card-body">
			    <a href="crudCart.php?cartProduct=Belajar+Dasar+Pemrograman+Web&cartPrice=100000&buy=buy" class="card-link btn btn-primary" role="button" style="width: 15rem;">Buy</a>
			  </div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 18rem;">
			  <img src="https://www.dicoding.com/images/original/academy/java_fundamental_logo_080119141920.jpg" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Memulai Pemrograman Dengan Java</h5>
			    <p class="card-text"><b>Rp.150.000,-</b></p>
			    <p class="card-text" style="height: 5rem;">Java adalah sebuah bahasa yang diciptakan oleh James Gosling di tahun 1990-an. Saat itu, paradigma Write Once, Run Anywhere (WORA) belum populer.</p>
			  </div>
			  <div class="card-body">
			    <a href="crudCart.php?cartProduct=Memulai+Pemrograman+Dengan+Java&cartPrice=150000&buy=buy" class="card-link btn btn-primary" role="button" style="width: 15rem;">Buy</a>
			  </div>
			</div>
		</div>
		<div class="col-sm-auto">
			<div class="card mx-auto" style="width: 18rem;">
			  <img src="https://www.dicoding.com/images/original/academy/memulai_pemrograman_dengan_python_logo_090719134021.jpg" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Memulai Pemrograman Dengan Python</h5>
			    <p class="card-text"><b>Rp.200.000,-</b></p>
			    <p class="card-text" style="height: 5rem;">Python adalah bahasa pemrograman interpretatif yang dapat digunakan pada berbagai platform dengan filosofi perancangan yang berfokus pada tingkat keterbacaan kode.</p>
			  </div>
			  <div class="card-body">
			    <a href="crudCart.php?cartProduct=Memulai+Pemrograman+Dengan+Python&cartPrice=200000&buy=buy" class="card-link btn btn-primary" role="button" style="width: 15rem;">Buy</a>
			  </div>
			</div>
		</div>
		<div class="col-sm-auto mt-3">
			<div class="card mx-auto" style="width: 18rem;">
			  <img src="https://www.dicoding.com/images/original/academy/belajar_version_control_logo_150419144733.jpg" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Source Code Management untuk Pemula</h5>
			    <p class="card-text"><b>Rp.250.000,-</b></p>
			    <p class="card-text" style="height: 5rem;">Di dalam dunia development aplikasi, source code management (version control) adalah salah satu tools yang bisa digunakan untuk berkolaborasi antara developer.</p>
			  </div>
			  <div class="card-body">
			    <a href="crudCart.php?cartProduct=Source+Code+Management+untuk+Pemula&cartPrice=250000&buy=buy" class="card-link btn btn-primary" role="button" style="width: 15rem;">Buy</a>
			  </div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <form method="post" action="login.php">
	      <div class="modal-header">
	        <h5 class="modal-title" id="loginModalLabel">Login</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	          <div class="form-group">
	            <label for="recipient-email" class="col-form-label">Email Address</label>
	            <input type="email" class="form-control" id="recipient-login-email" placeholder="Enter Email" name="emailLogin" required>
	          </div>
	          <div class="form-group">
	            <label for="recipient-password" class="col-form-label">Password</label>
	            <input type="password" class="form-control" id="recipient-login-password" placeholder="Password" name="passwordLogin" required>
	          </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        <input type="submit" class="btn btn-primary" value="Login" name="login">
	      </div>
	  	  </form>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <form method="post" action="register.php">
	      	<div class="modal-header">
	        	<h5 class="modal-title" id="registerModalLabel">Register</h5>
	        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          		<span aria-hidden="true">&times;</span>
	        	</button>
	      	</div>
	      	<div class="modal-body">
	          <div class="form-group">
	            <label for="recipient-email" class="col-form-label">Email Address</label>
	            <input type="email" class="form-control" id="recipient-register-email" placeholder="Enter Email" required name="regisEmail">
	          </div>
	          <div class="form-group">
	            <label for="recipient-username" class="col-form-label">Username</label>
	            <input type="text" class="form-control" id="recipient-register-username" placeholder="Enter Username" required name="regisUsername">
	          </div>
	          <div class="form-group">
	            <label for="recipient-password" class="col-form-label">Password</label>
	            <input type="password" class="form-control" id="recipient-register-password" placeholder="Password" required name="regisPassword">
	          </div>
	          <div class="form-group">
	            <label for="message-repassword" class="col-form-label">Confirm Password</label>
	            <input type="password" class="form-control" id="recipient-register-repassword" placeholder="Confirm Password" required name="regisConPassword">
	          </div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        	<input type="submit" class="btn btn-primary" value="Register" name="register">
	      	</div>
	  	  </form>
	    </div>
	  </div>
	</div>
	<footer class="page-footer font-small dark pt-4">
		<div class="footer text-center py-3">
			© EAD STORE<a href="#"> MUF</a>
		</div>
	</footer>
</body>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>